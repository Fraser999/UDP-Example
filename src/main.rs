#![feature(ip_addr, scoped, udp)]

extern crate rand;

use rand::random;
use std::error::Error;
use std::io;
use std::net::{Ipv4Addr, SocketAddr, UdpSocket};
use std::sync::{mpsc};
use std::thread;

const GUID_SIZE: usize = 16;
const MAGIC_SIZE: usize = 4;
const MAGIC: [u8; MAGIC_SIZE] = ['m' as u8, 'a' as u8, 'i' as u8, 'd' as u8];
pub type GUID = [u8; GUID_SIZE];

fn serialise_port(port: u16) -> [u8; 2] {
    [(port & 0xff) as u8, (port >> 8) as u8]
}

fn parse_port(data: &[u8]) -> u16 {
    (data[0] as u16) + ((data[1] as u16) << 8)
}

fn serialise_shutdown_value(shutdown_value: u64) -> [u8; 8] {
    [(shutdown_value & 0xff) as u8, (shutdown_value >> 8) as u8,
     (shutdown_value >> 16) as u8, (shutdown_value >> 24) as u8,
     (shutdown_value >> 32) as u8, (shutdown_value >> 40) as u8,
     (shutdown_value >> 48) as u8, (shutdown_value >> 56) as u8]
}

fn parse_shutdown_value(data: &[u8]) -> u64 {
    (data[0] as u64) + ((data[1] as u64) << 8) + ((data[2] as u64) << 16) +
    ((data[3] as u64) << 24) + ((data[4] as u64) << 32) + ((data[5] as u64) << 40) +
    ((data[6] as u64) << 48) + ((data[7] as u64) << 56)
}

pub fn seek_peers(port: u16, guid_to_avoid: Option<GUID>) -> io::Result<Vec<SocketAddr>> {
    // Send broadcast ping
    let socket = try!(UdpSocket::bind("0.0.0.0:55555"));
    try!(socket.set_broadcast(true));
    let my_udp_port = match try!(socket.local_addr()) {
        SocketAddr::V4(local_address) => local_address.port(),
        SocketAddr::V6(local_address) => local_address.port(),
    };

    let mut send_buffer: Vec<_> = From::from(&MAGIC[..]);
    let guid: Vec<_> = From::from(&guid_to_avoid.unwrap_or([0; GUID_SIZE])[..]);
    send_buffer.extend(guid.into_iter());

    try!(socket.send_to(&send_buffer[..], ("255.255.255.255", port)));

    let (tx, rx) = mpsc::channel::<SocketAddr>();

    // FIXME: This thread will never finish, eating one udp port
    // and few resources till the end of the program. I haven't
    // found a way to fix this in rust yet.
    let shutdown_value = random::<u64>();
                                                                                println!("Waiting for shutdown value: {}", shutdown_value);
    let _udp_response_thread = thread::scoped(move || -> io::Result<()> {
        loop {
            let mut buffer = [0u8; 8];
            let (size, source) = try!(socket.recv_from(&mut buffer));
            match size {
                2usize => {
                    let his_port = parse_port(&buffer);
                                                                            println!("Received port: {}", his_port);
                    let his_ep = SocketAddr::new(source.ip(), his_port);
                                                                            println!("Sending {:?} on tx channel", his_ep);
                    let _ = tx.send(his_ep);
                },
                8usize => {
                    if parse_shutdown_value(&buffer) == shutdown_value {
                                                                            println!("Received shutdown_value: {}", parse_shutdown_value(&buffer));
                        break
                    } else {
                                                                            println!("Received BAD shutdown_value: {}", parse_shutdown_value(&buffer));
                        continue
                    }
                },
                _ => {
                                                                                                println!("Received invalid size: {}", size);
                    continue
                },
            };
        };

        Ok(())
    });

    let _shutdown_thread = thread::scoped(move || {
        // Give peers time to respond.
        thread::sleep_ms(500);
        let killer_socket = UdpSocket::bind("0.0.0.0:0").unwrap();
        killer_socket.send_to(&serialise_shutdown_value(shutdown_value), ("127.0.0.1", my_udp_port));
    });

    let mut result = Vec::<SocketAddr>::new();

    loop {
        match rx.recv() {
            Ok(socket_addr) => {
                                                                                println!("RECEIVED {:?}", socket_addr);
                result.push(socket_addr)
            },
            Err(e) => {
                                                                                println!("ERROR RECEIVING: {}", e);
                break
            },
        }
    }

    //                                                                                 println!("About to join.");
    // let _ = udp_response_thread.join();
    //                                                                                 println!("About to joined.");
    // let _ = shutdown_thread.join();
                                                                                    println!("Joined.  Result - {:?}", result);
    Ok(result)
}


fn main() {
    let mut guid = [0; GUID_SIZE];
    for i in 0..GUID_SIZE {
        guid[i] = random::<u8>();
    }

    let t1 = thread::scoped(move || { let _ = seek_peers(9999, Some(guid)); });

    let t2 = thread::scoped(move || {
        let ip = Ipv4Addr::new(127, 0, 0, 1);
        let socket = UdpSocket::bind("0.0.0.0:0").unwrap();
        socket.send_to(&serialise_port(1234), (ip, 55555));
    });

    let t3 = thread::scoped(move || {
        let ip = Ipv4Addr::new(127, 0, 0, 1);
        let socket = UdpSocket::bind("0.0.0.0:0").unwrap();
        socket.send_to(&[9u8; 9], (ip, 55555));
    });

    let t4 = thread::scoped(move || {
        let ip = Ipv4Addr::new(127, 0, 0, 1);
        let socket = UdpSocket::bind("0.0.0.0:0").unwrap();
        socket.send_to(&serialise_shutdown_value(random::<u64>()), (ip, 55555));
    });

    for i in 0..10 {
        let t = thread::scoped(move || {
            let ip = Ipv4Addr::new(127, 0, 0, 1);
            let socket = UdpSocket::bind("0.0.0.0:0").unwrap();
            socket.send_to(&serialise_port((i * 11111) as u16), (ip, 55555));
        });
    }
}
